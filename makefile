include config

build:
	docker build -t $(BROKER_IMG_NAME) --build-arg version=$(VERSION) ./broker/
	docker build -t $(NAMESRV_IMG_NAME) --build-arg version=${VERSION} ./namesrv/

push: build
	docker push $(BROKER_IMG_NAME)
	docker push $(NAMESRV_IMG_NAME)

run:
	docker-compose -f docker-compose.yml up -d

down:
	docker-compose down

ui:
	docker-compose -f ./admin/docker-compose.yml up -d

uidown:
	docker-compose -f ./admin/docker-compose.yml down